(* ::Package:: *)

AppendTo[$Path , FileNameJoin[{DirectoryName[$InputFileName] , "wlmonad"}]];


BeginPackage["qbits`" , {"wlmonad`"}];


(****KP****)

kp::usage = "kp[a , b , c , ...]

Returns the kronecker product of one or more arrays or states a , b , c, ...

Attributes: Flat , OneIdentity
";

stateQ::usage = "stateQ[0]
stateQ[1]
stateQ[{1 , 0 , 1 , 1 , ...}]

Returns |0> = {1 , 0}, |1> = {0 , 1} or a kronecker product of one or more qbits |1> \[CircleTimes] |0> \[CircleTimes] |1> \[CircleTimes] |1> \[CircleTimes] ...
";

projectionQ::usage = "projectionQ[0]
projectionQ[1]
projectionQ[{\[Alpha] , \[Beta]}]

Returns the projection matrix onto |0>, |1> or a combination |state> = \[Alpha] |0> + \[Beta] |1>.
";

toQbits::usage = "toQbits[state_List]

Returns a representation of |state> using individual qbits. The list contains probability amplitudes
for alla combinations of qbit measurments. The total number of qbits is Log[2,Length[state]]. 
";

ket::usage = "ket[{0 , 1 , 1 , ...}]

Represents |0>\[CircleTimes]|1>\[CircleTimes]|1>\[CircleTimes]...
";


(****GATES****)

getSimulationList::usage = "getSimulation[circuit]

Returns data necessary for a sumulation of the circuit.
";

simulateQ::usage = "simulateQ[n][qc][initial]

Simulate quantum circuit -qc- using -n- iterations. The result
is a list of -n- measurments. The initial state is -initial-.
";

operateQ::usage = "operateQ[qc][initial]

Run the quantum circuit -qc- with initial state -initial-.
Returns a list:
	{stateQ , stateC}
with the evolved quantum state -stateQ- and the resulting
classical bits -stateC-	
";

getGraphics::usage = "getGraphics[circuit]

Returns the graphical representation of the circuit.
";

iG::usage = "iG

Represents the identity gate.
";

xG::usage = "xG

Represents the X gate.
";

yG::usage = "yG

Represents the Y gate.
";

zG::usage = "zG

Represents the Z gate.
";

hG::usage = "hG

Represents the H gate.
";

sG::usage = "sG

Represents the S gate.
";

tG::usage = "tG

Represents the T gate.
";

psG::usage = "psG[\[Phi]]

Represents a phase shift, \[Phi], gate.
";

rzG::usage = "rzG[\[Phi]]

Represents a rotation by \[Phi] around z.
";

ryG::usage = "ryG[\[Phi]]

Represents a rotation by \[Phi] around y.
";

rxG::usage = "rxG[\[Phi]]

Represents a rotation by \[Phi] around x.
";

controlledG::usage = "controlledG[gate]

Represents then controlled gate.
";

cG::usage = "cG[string]

Represents a comment. The string argument of this gate is a comment.

";

parG::usage = "parG[{g1 , g2 , ...}]

Represents a parallel gate g1\[CircleTimes]g2\[CircleTimes]...

";

gate::usage = "gate

Quantum gate pattern.
";


(****LAZYNESS****)

lazyList::usage = "lazyList

Lazy list for qbit lists.
";

take::usage = "take[part][l]

Lazy version of Part for lists of qbits.
";


(****MONAD****)

qC::usage = "qC

Quantum circuit.";

getQState::usage = "getQState[phi]

Returns the quentum mechanical state from the circuit result.";

getCState::usage = "getCState[phi]

Returns the classical state, measured values, from the circuit result.";


(****ACTIONS****)

newG::usage = "newG[g : gate][where]

Add new gate and attach it to qbits specified by where.";

newtG::usage = "newtG[g : gate][where]

Add new gate, hermitian transpose it, and attach it to qbits specified by where.";

newM::usage = "newM[{b1 , b2 , b3, ...}][where]

Add new measurment. Qbits specified by -where- will be measured in the |0>, |1>
	{1 , 0} , {0 , 1}
basis. Classical bits will be written to single bit registers
	b1, b2, b3, ...

TODO:
newM[{{b1 , base1} , {b2 , base2} , {b3 , base3}, ...}][where]

Alternatively 
	base1, base2, base3, ...
can be used to perform the measurnment on the qbits specified by where.
";


Begin["`Private`"];


(****KP****)

SetAttributes[kp , {Flat , OneIdentity}];

kp[c___?ArrayQ , a_?ArrayQ , b_?ArrayQ]:=kp[c,KroneckerProduct[a , b]]; 

kp[x_]:=x;

stateQ[0] = {1.0 , 0.0};

stateQ[1] = {0.0 , 1.0};

stateQ[l:{(0|1)...}]:= Flatten[kp@@(stateQ/@l)];

projectionQ[state:{_ , _}]:=Table[(Conjugate[stateQ[i]].state) (Conjugate[state].stateQ[j]) , {i , 0 , 1} , {j , 0 , 1}];

projectionQ[0] = projectionQ[stateQ[0]];

projectionQ[1] = projectionQ[stateQ[1]];

bits[bitn_][index_]:=Mod[Floor[(index-1)/2^(bitn-1)]  , 2];

allbits[n_][index_]:=Table[bits[bitn][index] , {bitn , n , 1 , -1}];

toQbits[state_]:=Partition[
					Riffle[
						state,
						ket/@(allbits[Log[2 , Length[state]]]/@Range[Length[state]])
					] , 
				2];


(****GATES****)

(*colors and line types for gates and transposed gates*)

normalRule = {modWhite->White , modBlack->Black , modSolid->EdgeForm[Black] , modFont->Gray , modGray->Black};

transposeRule = {modWhite->Black , modBlack->White , modSolid->EdgeForm[Black] , modFont->Gray , modGray->Black};

lineColor = Black;

lineType = EdgeForm[Black];

(*patterns*)

measurmentGate = mG[_];

singleBitGate = (iG|xG|yG|zG|hG|sG|tG|psG[_]);

gate = (iG|xG|yG|zG|hG|sG|tG|psG[_]|cG[{_String...}]|controlledG[_]|parG[_]|mG[_]);

(*helpers for serial gates*)

serialMatrix[n_][serialG[m_?ArrayQ , g_List][{i_}]]:=
		kp@@Table[
			If[ii == i , 
				m , 
				N[IdentityMatrix[2]]
			] , 
		{ii , 1 , n}]/;ArrayQ[m]&&Dimensions[m] == {2 , 2};
		
serialGraphics[n_][serialG[m_?ArrayQ , g_List][{i_}]]:=
		Table[
			If[ii == i , 
				Translate[g , {0 , -ii}] , 
				Translate[{lineColor, lineType , Line[{{-0.5 , 0},{0.5 , 0}}]} , {0 , -ii}]
			] , 
		{ii , 1 , n}]/;ArrayQ[m]&&Dimensions[m] == {2 , 2};
		
(*helpers for parallel gates*)

parallelMatrix[n_][parallelG[ms:{_?ArrayQ...}]][where:{_Integer...}]:=
		Module[{sorted , tab},
			(*{{1 , matrix for 1} , {3 , matrix for 3}, ...}*)
			sorted = Sort[
						Partition[
							Riffle[where , ms] , 
						2],
						#1[[1]] < #2[[1]]&
					];
			tab = Table[ 
						N[IdentityMatrix[2]],
					{ii , 1 , n}];
			Do[
				tab[[sorted[[ii,1]]]] = sorted[[ii , 2]];
			,{ii , 1 , Length[sorted]}];
			kp@@tab
		]/;Length[ms] == Length[where];
		
parallelGraphics[n_ , lns_][parallelG[gs:{_List...}]][where:{_Integer...}]:=
		Module[{sorted , tab , lines},
			(*{{1 , graphics for 1} , {3 , graphics for 3}, ...}*)
			sorted = Sort[
						Partition[
							Riffle[where , gs] , 
						2],
						#1[[1]] < #2[[1]]&
					];
			tab = Table[ 
						Translate[{lineColor,lineType , Line[{{-0.5 , 0},{0.5 , 0}}]} , {0 , -ii}],
					{ii , 1 , n}];
			Do[
				tab[[sorted[[ii,1]]]] = Translate[sorted[[ii , 2]] , {0 , -sorted[[ii , 1]]}];
			,{ii , 1 , Length[sorted]}];
			If[lns,
				lines = Table[
						{lineColor,lineType , Line[{{0 , -sorted[[ii,1]]},{0 , -sorted[[ii+1,1]]}}]}
					 , {ii ,1 , Length[sorted]-1}],
				lines = {}
			];
			{lines,tab}
		]/;Length[gs] == Length[where];
		
(*iG*)

matrix[iG] = SparseArray[N[IdentityMatrix[2]]];

graphics[iG] = {lineColor,lineType , Line[{{-0.5 , 0},{0.5 , 0}}]};

getSparse[n_][kpGate[iG][{i_}]]:= 
	serialMatrix[n][serialG[matrix[iG] , graphics[iG]][{i}]];
	
getGraphics[n_][grGate[iG][{i_}]]:=
	serialGraphics[n][serialG[matrix[iG] , graphics[iG]][{i}]];
	
(*xG*)

matrix[xG] = SparseArray[N[PauliMatrix[1]]];

graphics[xG] = {
					{lineColor,lineType , Line[{{-0.5 , 0},{0.5 , 0}}]},
					{modWhite , Disk[{0 , 0} , 0.125]},
					{modBlack,modSolid ,Circle[{0 , 0} , 0.125]} , 
					{modBlack,modSolid ,Line[{{0 , -0.125},{0 ,0.125}}]},
					{modBlack,modSolid ,Line[{{-0.125 , 0},{0.125 , 0}}]}
				};
				
getSparse[n_][kpGate[xG][{i_}]]:=
	serialMatrix[n][serialG[matrix[xG] , graphics[xG]][{i}]];
	
getGraphics[n_][grGate[xG][{i_}]]:=
	serialGraphics[n][serialG[matrix[xG] , graphics[xG]][{i}]];
	
(*yG*)

matrix[yG] = SparseArray[N[PauliMatrix[2]]];

graphics[yG] = {
					{lineColor,lineType , Line[{{-0.5 , 0},{0.5 , 0}}]},
					{modWhite ,modSolid, Rectangle[{-0.25 , -0.25},{0.25 , 0.25}]} , 
					{modFont , Text[Style["Y" , Medium] , {0.0 , 0.0} , {0,0}]}
				};
				
getSparse[n_][kpGate[yG][{i_}]]:=
	serialMatrix[n][serialG[matrix[yG] , graphics[yG]][{i}]];
	
getGraphics[n_][grGate[yG][{i_}]]:=
	serialGraphics[n][serialG[matrix[yG] , graphics[yG]][{i}]];
	
(*zG*)

matrix[zG] = SparseArray[N[PauliMatrix[3]]];

graphics[zG] = {
					{lineColor, lineType , Line[{{-0.5 , 0},{0.5 , 0}}]},
					{modWhite , modSolid,Rectangle[{-0.25 , -0.25},{0.25 , 0.25}]} , 
					{modFont , Text[Style["Z" , Medium] , {0.0 , 0.0} , {0,0}]}
				};
				
getSparse[n_][kpGate[zG][{i_}]]:=
	serialMatrix[n][serialG[matrix[zG] , graphics[zG]][{i}]];
	
getGraphics[n_][grGate[zG][{i_}]]:=
	serialGraphics[n][serialG[matrix[zG] , graphics[zG]][{i}]];
	
(*hG*)

matrix[hG] = 1.0/Sqrt[2.0] SparseArray[N[PauliMatrix[1] + PauliMatrix[3]]];

graphics[hG] = {
					{lineColor, lineType , Line[{{-0.5 , 0},{0.5 , 0}}]},
					{modWhite ,modSolid, Rectangle[{-0.25 , -0.25},{0.25 , 0.25}]} , 
					{modFont , Text[Style["H" , Medium] , {0.0 , 0.0} , {0,0}]}
				};
				
getSparse[n_][kpGate[hG][{i_}]]:=
	serialMatrix[n][serialG[matrix[hG] , graphics[hG]][{i}]];
	
getGraphics[n_][grGate[hG][{i_}]]:=
	serialGraphics[n][serialG[matrix[hG] , graphics[hG]][{i}]];
	
(*sG*)

matrix[sG] = SparseArray[N[{{1 , 0},{0 , I}}]];

graphics[sG] = {
					{lineColor, lineType , Line[{{-0.5 , 0},{0.5 , 0}}]},
					{modWhite ,modSolid, Rectangle[{-0.25 , -0.25},{0.25 , 0.25}]} , 
					{modFont , Text[Style["S" , Medium] , {0.0 , 0.0} , {0,0}]}
				};
				
getSparse[n_][kpGate[sG][{i_}]]:=
	serialMatrix[n][serialG[matrix[sG] , graphics[sG]][{i}]];
	
getGraphics[n_][grGate[sG][{i_}]]:=
	serialGraphics[n][serialG[matrix[sG] , graphics[sG]][{i}]];	
	
(*tG*)

matrix[tG] = SparseArray[N[{{1 , 0},{0 , Exp[I \[Pi]/4]}}]];

graphics[tG] = {
					{lineColor, lineType , Line[{{-0.5 , 0},{0.5 , 0}}]},
					{modWhite ,modSolid, Rectangle[{-0.25 , -0.25},{0.25 , 0.25}]} , 
					{modFont , Text[Style["S" , Medium] , {0.0 , 0.0} , {0,0}]}
				};
				
getSparse[n_][kpGate[tG][{i_}]]:=
	serialMatrix[n][serialG[matrix[tG] , graphics[tG]][{i}]];
	
getGraphics[n_][grGate[tG][{i_}]]:=
	serialGraphics[n][serialG[matrix[tG] , graphics[tG]][{i}]];	
	
(*psG*)

matrix[psG[\[Phi]_]] := SparseArray[N[{{1 , 0},{0 , Exp[I \[Phi]]}}]];

graphics[psG[\[Phi]_]] := {
					{lineColor, lineType , Line[{{-0.5 , 0},{0.5 , 0}}]},
					{modWhite ,modSolid, Rectangle[{-0.25 , -0.25},{0.25 , 0.25}]} , 
					{modFont , Text[Style["P("<>ToString[\[Phi]]<>")" , Medium] , {0.0 , 0.0} , {0,0}]}
				};
				
getSparse[n_][kpGate[psG[\[Phi]_]][{i_}]]:=
	serialMatrix[n][serialG[matrix[psG[\[Phi]]] , graphics[psG[\[Phi]]]][{i}]];
	
getGraphics[n_][grGate[psG[\[Phi]_]][{i_}]]:=
	serialGraphics[n][serialG[matrix[psG[\[Phi]]] , graphics[psG[\[Phi]]]][{i}]];	
	
(*rzG*)

matrix[rzG[\[Phi]_]] := SparseArray[N[{{Exp[-I \[Phi]/2] , 0},{0,Exp[I \[Phi]/2]}}]];

graphics[rzG[\[Phi]_]] := {
					{lineColor, lineType , Line[{{-0.5 , 0},{0.5 , 0}}]},
					{modWhite ,modSolid, Rectangle[{-0.25 , -0.25},{0.25 , 0.25}]} , 
					{modFont , Text[Style["\!\(\*SubscriptBox[\(R\), \(z\)]\)("<>ToString[\[Phi]]<>")" , Medium] , {0.0 , 0.0} , {0,0}]}
				};
				
getSparse[n_][kpGate[rzG[\[Phi]_]][{i_}]]:=
	serialMatrix[n][serialG[matrix[rzG[\[Phi]]] , graphics[rzG[\[Phi]]]][{i}]];
	
getGraphics[n_][grGate[rzG[\[Phi]_]][{i_}]]:=
	serialGraphics[n][serialG[matrix[rzG[\[Phi]]] , graphics[rzG[\[Phi]]]][{i}]];	
	
(*ryG*)

matrix[ryG[\[Phi]_]] := SparseArray[N[{{Cos[\[Phi]/2] , -Sin[\[Phi]/2]},{Sin[\[Phi]/2],Cos[\[Phi]/2]}}]];

graphics[ryG[\[Phi]_]] := {
					{lineColor, lineType , Line[{{-0.5 , 0},{0.5 , 0}}]},
					{modWhite ,modSolid, Rectangle[{-0.25 , -0.25},{0.25 , 0.25}]} , 
					{modFont , Text[Style["\!\(\*SubscriptBox[\(R\), \(z\)]\)("<>ToString[\[Phi]]<>")" , Medium] , {0.0 , 0.0} , {0,0}]}
				};
				
getSparse[n_][kpGate[ryG[\[Phi]_]][{i_}]]:=
	serialMatrix[n][serialG[matrix[ryG[\[Phi]]] , graphics[ryG[\[Phi]]]][{i}]];
	
getGraphics[n_][grGate[ryG[\[Phi]_]][{i_}]]:=
	serialGraphics[n][serialG[matrix[ryG[\[Phi]]] , graphics[ryG[\[Phi]]]][{i}]];	
	
(*rxG*)

matrix[rxG[\[Phi]_]] := SparseArray[N[{{Cos[\[Phi]/2] , -I Sin[\[Phi]/2]},{-I Sin[\[Phi]/2],Cos[\[Phi]/2]}}]];

graphics[rxG[\[Phi]_]] := {
					{lineColor, lineType , Line[{{-0.5 , 0},{0.5 , 0}}]},
					{modWhite ,modSolid, Rectangle[{-0.25 , -0.25},{0.25 , 0.25}]} , 
					{modFont , Text[Style["\!\(\*SubscriptBox[\(R\), \(z\)]\)("<>ToString[\[Phi]]<>")" , Medium] , {0.0 , 0.0} , {0,0}]}
				};
				
getSparse[n_][kpGate[rxG[\[Phi]_]][{i_}]]:=
	serialMatrix[n][serialG[matrix[rxG[\[Phi]]] , graphics[rxG[\[Phi]]]][{i}]];
	
getGraphics[n_][grGate[rxG[\[Phi]_]][{i_}]]:=
	serialGraphics[n][serialG[matrix[rxG[\[Phi]]] , graphics[rxG[\[Phi]]]][{i}]];	
	
(*mG[_] - measurnmnet, not technically a gate*)

graphics[mG[what_Integer]] := {
					{lineColor, lineType , Line[{{-0.5 , 0},{0.5 , 0}}]},
					{modWhite ,modSolid, Rectangle[{-0.25 , -0.25},{0.25 , 0.25}]},
					{modWhite , modSolid , Disk[{0 , 0} , 0.2]} , 
					{lineColor, lineType , Arrowheads[Small],Arrow[{{0 , 0},{0.12727922061357855`,0.12727922061357855`}}]},
					{modFont , Text[Style[ToString[what] , Medium] , {0.0 , 0.0} , {0,0}]}
				};
				
getGraphics[n_][grGate[mG[what:{_Integer...}]][where_]]:=
	parallelGraphics[n , False][parallelG[graphics/@(mG/@what)]][where];
	
(*controlledG*)

matrix[controlledG[g_]] := SparseArray[matrix[g]];

graphics[controlledG[g_]] := {{lineColor, lineType , Line[{{-0.5 , 0},{0.5 , 0}}]},{modBlack , Disk[{0,0} , 0.125]},{modWhite , Circle[{0,0} , 0.125]}};

getSparse[n_][kpGate[controlledG[g_]][where:{_Integer...}]]:=
		Module[{ms1 , ms0},
			ms1 = Join[{matrix[g]} , Table[projectionQ[1] , {ii , 2 , Length[where]}]];
			ms0 = Join[{matrix[iG]} , Table[projectionQ[0] , {ii , 2 , Length[where]}]];
			parallelMatrix[n][parallelG[ms1]][where] +
			parallelMatrix[n][parallelG[ms0]][where]
		]/;Length[where]>=2;
		
getGraphics[n_][grGate[controlledG[g_]][where:{_Integer...}]]:=
		Module[{gs},
			gs = graphics/@Join[{g} , Table[controlledG[g] , {ii , 2 , Length[where]}]];
			parallelGraphics[n , True][parallelG[gs]][where]
		]/;Length[where]>=2;
		
(*cG*)

matrix[cG[{_String...}]] = SparseArray[N[IdentityMatrix[2]]];

getSparse[n_][kpGate[cG[comment:{_String...}]][where:{_Integer...}]]:=
	kp@@Table[matrix[cG[comment]] , {ii , 1 , n}]/;Length[comment] == Length[where];
	
graphics[cG[comment_String]] := 
			{
				{lineColor , lineType , Line[{{-0.5 , 0},{0.5 , 0}}]},
				{White , EdgeForm[None] , Rectangle[{-0.5 , -0.5},{0.5 , 0.5}]} , 
				{modFont , Text[Style[comment , Medium] , {0.0 , 0.0} , {0,0}]}
			};
			
getGraphics[n_][grGate[cG[comment:{_String...}]][where:{_Integer...}]]:=
			Module[{gs},
				gs = graphics/@(cG/@comment);
				parallelGraphics[n , False][parallelG[gs]][where]
			]/;Length[comment] == Length[where];
			
(*parG*)

getSparse[n_][kpGate[parG[gs:{singleBitGate...}]][where:{_Integer...}]]:=
		parallelMatrix[n][parallelG[matrix/@gs]][where]/;Length[where]==Length[gs];
		
getGraphics[n_][grGate[parG[gs:{singleBitGate...}]][where:{_Integer...}]]:=
		parallelGraphics[n , False][parallelG[graphics/@gs]][where]/;Length[where]==Length[gs];
		
(**)

getSparse[n_][kptGate[gate:gate][where:{_Integer...}]]:=ConjugateTranspose[getSparse[n][kpGate[gate][where]]];

getGraphics[n_][grtGate[gate:gate][where:{_Integer...}]]:=(getGraphics[n][grGate[gate][where]])/.transposeRule;


(****LAZYNESS****)

lazyList[][l:{_Integer...}]:=l;

lazyList[part:{_Integer...}..][l:{_Integer...}..]:=
	Join@@(Part@@#&/@Partition[Riffle[{l} , {part}] ,2])/;Length[{part}] == Length[{l}];
	
take[part__][l__]:=lazyList[part][l]/;Length[{part}] == Length[{l}];


(****MONAD****)

graphicsRepresentation = _List;

sparseRepresentation = _List;

qCRest = {
			_ (*total number of qbits*) , 
			graphicsRepresentation , 
			sparseRepresentation , 
			_ (*total number of classical bits*)
		};
		
pattern[qC] = qC[_ , qCRest];

return[qC][out_]:=qC[out , {0,{},{id} , 0}];(*TODO, pattern to out*)

grab[ma:pattern[qC]]:=ma[[1]];

rest[ma:pattern[qC]]:=ma[[2]];

join[
	{
		na_ , 
		ag : graphicsRepresentation,
		as : sparseRepresentation,
		ca_
	} , 
	{
		nb_ , 
		bg : graphicsRepresentation,
		bs : sparseRepresentation,
		cb_
	}
]:= {Max[na , nb] , Join[ag , bg] , Join[as , bs],Max[ca , cb]};

bind[qC][ma_ , aTomb_]:= 
		With[
			{val = aTomb[grab[ma]]}, 
			qC[grab[val] , join[rest[val],rest[ma]]]
		];


(****HELPER FUNCTIONS****)

operQ[oper_SparseArray][{qstate_ , cstate_}]:={oper.qstate , cstate};

mesQ[n_ , sub_List , m_][{qstate_ , cstate_}]:=Module[{projections , replacements,prob , choice , new},
	projections = sub[[;; , 2]];
	replacements = sub[[;; , 1]];
	prob = Function[oper , Conjugate[oper.qstate].(oper.qstate)]/@projections;
	choice = RandomChoice[prob -> Range[Length[sub]]];
	new = projections[[choice]].qstate;
	new = new / Sqrt[Conjugate[new].new];
	{new , cstate/.replacements[[choice]]}
];

getGraphics[qc:pattern[qC]]:=
	Graphics[
		MapIndexed[
			Translate[#1 , {-#2[[1]] , 0}]&,
			(getGraphics[qc[[2,1]]]/@Flatten[qc[[2,2]]])
		]/.normalRule , 
	PlotRange->All];
	
getSparse[n_Integer , m_Integer][qc:{(kpGate[_][_]|kptGate[_][_])...}]:=operQ[Dot@@(getSparse[n]/@qc)];

getSparse[n_Integer , m_Integer][qc:{measurnmentQ[what_,where_]}]:= Module[{results , idk , klist},
	results = Tuples[{0 , 1} , Length[what]];
	idk = Table[SparseArray[N[IdentityMatrix[2]]] , {ii , 1 , n}];
	mesQ[n,Table[
		klist = idk;
		Do[
			klist[[where[[jj]]]] = SparseArray[projectionQ[results[[ii , jj]]]];
		,{jj , 1 , Length[where]}];
		{Function[r , bit[r[[1]]]->r[[2]]]/@Partition[Riffle[what,results[[ii]]] , 2], kp@@klist}
	,{ii , 1 , Length[results]}]
	,m]
];

getSimulationList[qc:pattern[qC]]:=Module[{n , m , operators},
		n = qc[[2,1]];
		m = qc[[2,4]];
		operators = DeleteCases[qc[[2,3]] , id];
		operators = Split[operators , Not[Or[Head[#1]===measurnmentQ , Head[#2]===measurnmentQ]]&];
		simList@@(getSparse[n , m]/@operators)
	];
	
simulateQ[n_][qc:pattern[qC]][initial_]:=
		Module[{simList , simListReversed,initialC , stateQ , stateC},
			simList = getSimulationList[qc];
			initialC = Table[bit[ii] , {ii , 1 , qc[[2,4]]}];
			simListReversed = simList//Reverse;
			Table[
				stateQ = initial;
				stateC = initialC;
				Do[
					{stateQ , stateC} = simListReversed[[ii]][{stateQ , stateC}];
				,{ii , 1 , Length[simListReversed]}];
				stateC
			,{n}]
		];
		
operateQ[qc:pattern[qC]][initial_]:=
		Module[{simList , simListReversed,initialC , stateQ , stateC},
			simList = getSimulationList[qc];
			initialC = Table[bit[ii] , {ii , 1 , qc[[2,4]]}];
			simListReversed = simList//Reverse;
			stateQ = initial;
			stateC = initialC;
			Do[
				{stateQ , stateC} = simListReversed[[ii]][{stateQ , stateC}];
			,{ii , 1 , Length[simListReversed]}];
			{stateQ,stateC}
		];
		
getQState[{quantumState_ , classicalState_}]:=quantumState;

getCState[{quantumState_ , classicalState_}]:=classicalState;


(****ACTIONS****)

newG[g:gate][where_]:= qC[lazyList[][where] , {Max[where],{grGate[g][where]},{kpGate[g][where]} , 0}];

newtG[g:gate][where_]:= qC[lazyList[][where] , {Max[where],{grtGate[g][where]},{kptGate[g][where]} , 0}];

newM[what_][where_]:= qC[lazyList[][where] , {Max[where],{grGate[mG[what]][where]},{measurnmentQ[what , where]},Max[what]}];


End[];


EndPackage[];
